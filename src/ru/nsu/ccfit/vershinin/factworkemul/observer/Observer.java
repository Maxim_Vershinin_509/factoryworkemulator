package ru.nsu.ccfit.vershinin.factworkemul.observer;

public interface Observer {
    void update(Observable.StoreUpdater storeUpdater);
}
