package ru.nsu.ccfit.vershinin.factworkemul.observer;

public interface Observable {
    enum StoreUpdater {
        STORE_CAR_COUNT, STORE_ACCESSORY_COUNT, STORE_BODY_COUNT,
        STORE_MOTOR_COUNT, COUNT_ACCESSORIES, COUNT_BODIES, COUNT_MOTORS,
        COUNT_CARS
    }

    void registerObserver(Observer o);

    void notifyObserver(StoreUpdater storeUpdater);

    void removeObserver(Observer o);
}
