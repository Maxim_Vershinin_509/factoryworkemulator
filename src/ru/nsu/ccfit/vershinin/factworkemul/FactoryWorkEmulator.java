package ru.nsu.ccfit.vershinin.factworkemul;

import ru.nsu.ccfit.vershinin.factworkemul.controller.Controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class FactoryWorkEmulator {

    public static void main(String[] args) {
        final Properties config = new Properties();
        try {
            config.load(new FileInputStream("./res/config.properties"));
        } catch (IOException ex) {
            System.err.println(ex.getLocalizedMessage());
            return;
        }
        Controller controller = new Controller(config);
    }
}
