package ru.nsu.ccfit.vershinin.factworkemul.controller;

public interface ControllerInterface {
    void setAccessorySuppliersTimeWork(int timeWork);
    void setBodySuppliersTimeWork(int timeWork);
    void setMotorSuppliersTimeWork(int timeWork);
    void setMachineBuildersTimeWork(int timeWork);
    void setDealersTimeWork(int timeWork);
    void exit();
}
