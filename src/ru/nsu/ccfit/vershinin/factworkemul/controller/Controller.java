package ru.nsu.ccfit.vershinin.factworkemul.controller;

import ru.nsu.ccfit.vershinin.factworkemul.factory.Factory;
import ru.nsu.ccfit.vershinin.factworkemul.view.Viewer;

import java.util.Properties;

public class Controller implements ControllerInterface {
    private final Factory factory;

    @Override
    public void setAccessorySuppliersTimeWork(int timeWork) {
        factory.setAccessorySuppliersTimeWork(timeWork);
    }

    @Override
    public void setBodySuppliersTimeWork(int timeWork) {
        factory.setBodySuppliersTimeWork(timeWork);
    }

    @Override
    public void setMotorSuppliersTimeWork(int timeWork) {
        factory.setMotorSuppliersTimeWork(timeWork);
    }

    @Override
    public void setMachineBuildersTimeWork(int timeWork) {
        factory.setMachineBuildersTimeWork(timeWork);
    }

    @Override
    public void setDealersTimeWork(int timeWork) {
        factory.setDealersTimeWork(timeWork);
    }

    @Override
    public void exit() {
        factory.exit();
    }

    public Controller(Properties config) {
        new Viewer(factory = new Factory(config), this);
    }
}
