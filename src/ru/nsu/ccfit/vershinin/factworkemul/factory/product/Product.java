package ru.nsu.ccfit.vershinin.factworkemul.factory.product;

public interface Product {
    int getId();
}
