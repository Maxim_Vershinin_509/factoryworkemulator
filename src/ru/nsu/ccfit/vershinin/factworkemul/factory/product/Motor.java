package ru.nsu.ccfit.vershinin.factworkemul.factory.product;

public class Motor implements Product {
    private final int id;
    private static int globalId = 0;

    public Motor() {
        synchronized (Motor.class) {
            id = globalId++;
        }
    }

    public int getId() {
        return id;
    }
}
