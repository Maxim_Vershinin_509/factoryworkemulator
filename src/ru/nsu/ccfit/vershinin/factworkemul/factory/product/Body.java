package ru.nsu.ccfit.vershinin.factworkemul.factory.product;

public class Body implements Product {
    private final int id;
    private static int globalId = 0;

    public Body() {
        synchronized (Body.class) {
            id = globalId++;
        }
    }

    public int getId() {
        return id;
    }
}
