package ru.nsu.ccfit.vershinin.factworkemul.factory.product;

public class Accessory implements Product {
    private final int id;
    private static int globalId = 0;

    public Accessory() {
        synchronized (Accessory.class) {
            id = globalId++;
        }
    }

    public int getId() {
        return id;
    }
}
