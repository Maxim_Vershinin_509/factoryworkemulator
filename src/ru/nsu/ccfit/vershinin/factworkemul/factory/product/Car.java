package ru.nsu.ccfit.vershinin.factworkemul.factory.product;

public class Car implements Product {
    private final int id;
    private static int globalId = 0;
    private final Product motor;
    private final Product body;
    private final Product accessory;

    public Car(Motor motor, Body body, Accessory accessory) {
        synchronized (Car.class) {
            id = globalId++;
        }
        this.motor = motor;
        this.body = body;
        this.accessory = accessory;
    }

    public int getId() {
        return id;
    }

    public int getMotorId() {
        return motor.getId();
    }

    public int getBodyId() {
        return body.getId();
    }

    public int getAccessoryId() {
        return accessory.getId();
    }
}
