package ru.nsu.ccfit.vershinin.factworkemul.factory.store;

import ru.nsu.ccfit.vershinin.factworkemul.factory.product.Motor;
import ru.nsu.ccfit.vershinin.factworkemul.observer.Observable;

public class StoreMotor extends Store {

    public StoreMotor(int MaxSize, Observable factory) {
        super(MaxSize, factory);
    }

    public void addMotor(Motor motor) throws InterruptedException {
        addProduct(motor);
        updateInfoAboutStore(Observable.StoreUpdater.STORE_MOTOR_COUNT);
        updateInfoAboutStore(Observable.StoreUpdater.COUNT_MOTORS);
    }

    public Motor getMotor() throws InterruptedException {
        Motor motor = (Motor)getProduct();
        updateInfoAboutStore(Observable.StoreUpdater.STORE_MOTOR_COUNT);
        return motor;
    }
}
