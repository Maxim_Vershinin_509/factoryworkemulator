package ru.nsu.ccfit.vershinin.factworkemul.factory.store;

import ru.nsu.ccfit.vershinin.factworkemul.factory.product.Body;
import ru.nsu.ccfit.vershinin.factworkemul.observer.Observable;

public class StoreBody extends Store {

    public StoreBody(int MaxSize, Observable factory) {
        super(MaxSize, factory);
    }

    public void addBody(Body body) throws InterruptedException {
        addProduct(body);
        updateInfoAboutStore(Observable.StoreUpdater.STORE_BODY_COUNT);
        updateInfoAboutStore(Observable.StoreUpdater.COUNT_BODIES);
    }

    public Body getBody() throws InterruptedException {
        Body body = (Body)getProduct();
        updateInfoAboutStore(Observable.StoreUpdater.STORE_BODY_COUNT);
        return body;
    }
}
