package ru.nsu.ccfit.vershinin.factworkemul.factory.store;

import ru.nsu.ccfit.vershinin.factworkemul.factory.product.Product;
import ru.nsu.ccfit.vershinin.factworkemul.observer.Observable;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class Store {
    private final ArrayList<Product> products = new ArrayList<>();
    private final int MaxSize;
    private AtomicInteger countProduct = new AtomicInteger(0);
    private Observable factory;

    Store(int MaxSize, Observable factory) {
        this.MaxSize = MaxSize;
        this.factory = factory;
    }

    private boolean isBusy() {
        synchronized (products) {
            return MaxSize == products.size();
        }
    }

    void addProduct(Product product) throws InterruptedException {
        while (true) {
            synchronized (products) {
                if (isBusy()) {
                    products.wait();
                    continue;
                }
                products.add(product);
                products.notifyAll();
                break;
            }
        }
        countProduct.incrementAndGet();
    }

    Product getProduct() throws InterruptedException {
        Product product;
        while (true) {
            synchronized (products) {
                if (products.isEmpty()) {
                    products.wait();
                    continue;
                }
                product = products.get(0);
                products.remove(0);
                products.notifyAll();
                break;
            }
        }
        return product;

    }

    public int getCount() {
        synchronized (products) {
            return products.size();
        }
    }

    public int getCountProduct() {
        return countProduct.intValue();
    }

    void updateInfoAboutStore(Observable.StoreUpdater storeUpdater) {
        factory.notifyObserver(storeUpdater);
    }
}
