package ru.nsu.ccfit.vershinin.factworkemul.factory.store;

import ru.nsu.ccfit.vershinin.factworkemul.factory.product.Accessory;
import ru.nsu.ccfit.vershinin.factworkemul.observer.Observable;

public class StoreAccessory extends Store {

    public StoreAccessory(int MaxSize, Observable factory) {
        super(MaxSize, factory);
    }

    public void addAccessory(Accessory accessory) throws InterruptedException {
        addProduct(accessory);
        updateInfoAboutStore(Observable.StoreUpdater.STORE_ACCESSORY_COUNT);
        updateInfoAboutStore(Observable.StoreUpdater.COUNT_ACCESSORIES);
    }

    public Accessory getAccessory() throws InterruptedException {
        Accessory accessory = (Accessory)getProduct();
        updateInfoAboutStore(Observable.StoreUpdater.STORE_ACCESSORY_COUNT);
        return accessory;
    }
}
