package ru.nsu.ccfit.vershinin.factworkemul.factory.store;

import ru.nsu.ccfit.vershinin.factworkemul.factory.product.Car;
import ru.nsu.ccfit.vershinin.factworkemul.observer.Observable;

public class StoreCar extends Store {

    public StoreCar(int MaxSize, Observable factory) {
        super(MaxSize, factory);
    }

    public void addCar(Car car) throws InterruptedException {
        addProduct(car);
        updateInfoAboutStore(Observable.StoreUpdater.STORE_CAR_COUNT);
        updateInfoAboutStore(Observable.StoreUpdater.COUNT_CARS);
    }

    public Car getCar() throws InterruptedException {
        Car car = (Car)getProduct();
        updateInfoAboutStore(Observable.StoreUpdater.STORE_CAR_COUNT);
        return car;
    }
}
