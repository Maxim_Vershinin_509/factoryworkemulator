package ru.nsu.ccfit.vershinin.factworkemul.factory;

import ru.nsu.ccfit.vershinin.factworkemul.factory.store.*;
import ru.nsu.ccfit.vershinin.factworkemul.factory.worker.*;
import ru.nsu.ccfit.vershinin.factworkemul.observer.Observable;
import ru.nsu.ccfit.vershinin.factworkemul.observer.Observer;
import ru.nsu.ccfit.vershinin.factworkemul.threadpool.ThreadPool;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Factory implements Observable {
    private final ArrayList<Worker> accessorySuppliers = new ArrayList<>();
    private final ArrayList<Worker> bodySuppliers = new ArrayList<>();
    private final ArrayList<Worker> motorSuppliers = new ArrayList<>();
    private final ArrayList<Worker> machineBuilders = new ArrayList<>();
    private final ArrayList<Worker> dealers = new ArrayList<>();
    private StoreAccessory storeAccessory;
    private StoreBody storeBody;
    private StoreMotor storeMotor;
    private StoreCar storeCar;
    private final ThreadPool threadPool = new ThreadPool();
    private final List<Observer> observers = new ArrayList<>();

    public Factory(Properties config) {
        int storeAccessoryMaxSize = Integer.parseInt(config.getProperty("StoreAccessoryMaxSize"));
        int storeBodyMaxSize = Integer.parseInt(config.getProperty("StoreBodyMaxSize"));
        int storeMotorMaxSize = Integer.parseInt(config.getProperty("StoreMotorMaxSize"));
        int storeCarMaxSize = Integer.parseInt(config.getProperty("StoreCarMaxSize"));
        int countAccessorySuppliers = Integer.parseInt(config.getProperty("AccessorySuppliers"));
        int countBodySuppliers = Integer.parseInt(config.getProperty("BodySuppliers"));
        int countMotorSuppliers = Integer.parseInt(config.getProperty("MotorSuppliers"));
        int countMachineBuilders = Integer.parseInt(config.getProperty("MachineDealers"));
        int countDealers = Integer.parseInt(config.getProperty("Dealers"));
        boolean isLogFile = Boolean.parseBoolean(config.getProperty("LogFile"));

        storeAccessory = new StoreAccessory(storeAccessoryMaxSize, this);
        for (int i = 0; i < countAccessorySuppliers; i++) {
            accessorySuppliers.add(new AccessorySupplier(storeAccessory));
        }

        storeBody = new StoreBody(storeBodyMaxSize, this);
        for (int i = 0; i < countBodySuppliers; i++) {
            bodySuppliers.add(new BodySupplier(storeBody));
        }

        storeMotor = new StoreMotor(storeMotorMaxSize, this);
        for (int i = 0; i < countMotorSuppliers; i++) {
            motorSuppliers.add(new MotorSupplier(storeMotor));
        }
        storeCar = new StoreCar(storeCarMaxSize, this);

        for (int i = 0; i < countMachineBuilders; i++) {
            machineBuilders.add(new MachineBuilder(storeAccessory, storeBody, storeMotor, storeCar));
        }

        for (int i = 0; i < countDealers; i++) {
            dealers.add(new Dealer(storeCar, i, isLogFile));
        }

        for (Runnable r: accessorySuppliers) {
            threadPool.addTask(r);
        }
        for (Runnable r: bodySuppliers) {
            threadPool.addTask(r);
        }
        for (Runnable r: motorSuppliers) {
            threadPool.addTask(r);
        }
        for (Runnable r: machineBuilders) {
            threadPool.addTask(r);
        }
        for (Runnable r: dealers) {
            threadPool.addTask(r);
        }

    }

    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObserver(StoreUpdater storeUpdater) {
        for (Observer o : observers) {
            o.update(storeUpdater);
        }
    }

    public int getCountAccessories() {
        return storeAccessory.getCountProduct();
    }

    public int getCountBodies() {
        return storeBody.getCountProduct();
    }

    public int getCountMotors() {
        return storeMotor.getCountProduct();
    }

    public int getCountCars() {
        return storeCar.getCountProduct();
    }

    public int getStoreAccessoryCount() {
        return storeAccessory.getCount();
    }

    public int getStoreBodyCount() {
        return storeBody.getCount();
    }

    public int getStoreMotorCount() {
        return storeMotor.getCount();
    }

    public int getStoreCarCount() {
        return storeCar.getCount();
    }

    public void exit() {
        threadPool.closeThreads();
    }

    public void setAccessorySuppliersTimeWork(int timeWork) {
        for (Worker w: accessorySuppliers) {
            w.setTimeWork(timeWork);
        }
    }

    public void setBodySuppliersTimeWork(int timeWork) {
        for (Worker w: bodySuppliers) {
            w.setTimeWork(timeWork);
        }
    }

    public void setMotorSuppliersTimeWork(int timeWork) {
        for (Worker w: motorSuppliers) {
            w.setTimeWork(timeWork);
        }
    }

    public void setMachineBuildersTimeWork(int timeWork) {
        for (Worker w: machineBuilders) {
            w.setTimeWork(timeWork);
        }
    }

    public void setDealersTimeWork(int timeWork) {
        for (Worker w: dealers) {
            w.setTimeWork(timeWork);
        }
    }

}
