package ru.nsu.ccfit.vershinin.factworkemul.factory.worker;

import ru.nsu.ccfit.vershinin.factworkemul.factory.product.Motor;
import ru.nsu.ccfit.vershinin.factworkemul.factory.store.StoreMotor;

public class MotorSupplier extends Worker{
    private final StoreMotor storeMotor;

    public MotorSupplier(StoreMotor storeMotor) {
        this.storeMotor = storeMotor;
    }

    @Override
    public void run() {

        while (true) {
            try {
                Thread.sleep(timeWork);
                storeMotor.addMotor(new Motor());
            } catch (InterruptedException ex) {
                break;
            }
        }

    }
}
