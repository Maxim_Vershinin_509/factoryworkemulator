package ru.nsu.ccfit.vershinin.factworkemul.factory.worker;

import ru.nsu.ccfit.vershinin.factworkemul.factory.product.Accessory;
import ru.nsu.ccfit.vershinin.factworkemul.factory.store.StoreAccessory;

public class AccessorySupplier extends Worker {
    private final StoreAccessory storeAccessory;

    public AccessorySupplier(StoreAccessory storeAccessory) {
        this.storeAccessory = storeAccessory;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(timeWork);
                storeAccessory.addAccessory(new Accessory());
            } catch (InterruptedException ex) {
                break;
            }
        }
    }
}
