package ru.nsu.ccfit.vershinin.factworkemul.factory.worker;

public abstract class Worker implements Runnable {
    volatile int timeWork = 1000;

    public void setTimeWork(int timeWork) {
        this.timeWork = timeWork;
    }
}
