package ru.nsu.ccfit.vershinin.factworkemul.factory.worker;

import ru.nsu.ccfit.vershinin.factworkemul.factory.product.Car;
import ru.nsu.ccfit.vershinin.factworkemul.factory.store.*;

public class MachineBuilder extends Worker{
    private final StoreAccessory storeAccessory;
    private final StoreBody storeBody;
    private final StoreMotor storeMotor;
    private final StoreCar storeCar;

    public MachineBuilder(StoreAccessory storeAccessory, StoreBody storeBody, StoreMotor storeMotor, StoreCar storeCar) {
        this.storeAccessory = storeAccessory;
        this.storeBody = storeBody;
        this.storeMotor = storeMotor;
        this.storeCar = storeCar;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(timeWork);
                storeCar.addCar(new Car(storeMotor.getMotor(), storeBody.getBody(), storeAccessory.getAccessory()));
            } catch (InterruptedException ex) {
                break;
            }
        }
    }

}
