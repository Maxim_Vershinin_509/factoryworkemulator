package ru.nsu.ccfit.vershinin.factworkemul.factory.worker;

import ru.nsu.ccfit.vershinin.factworkemul.factory.product.Body;
import ru.nsu.ccfit.vershinin.factworkemul.factory.store.StoreBody;

public class BodySupplier extends Worker {
    private final StoreBody storeBody;

    public BodySupplier(StoreBody storeBody) {
        this.storeBody = storeBody;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(timeWork);
                storeBody.addBody(new Body());
            } catch (InterruptedException ex) {
                break;
            }
        }
    }
}
