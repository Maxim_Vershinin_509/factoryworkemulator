package ru.nsu.ccfit.vershinin.factworkemul.factory.worker;

import ru.nsu.ccfit.vershinin.factworkemul.factory.product.Car;
import ru.nsu.ccfit.vershinin.factworkemul.factory.store.StoreCar;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Date;

public class Dealer extends Worker {
    private final int numberOfDealer;
    private final StoreCar storeCar;
    private static BufferedWriter logFile;

    public Dealer(StoreCar storeCar, int numberOfDealer, boolean isLogFile) {
        this.storeCar = storeCar;
        this.numberOfDealer = numberOfDealer;
        if (isLogFile) {
            try {
                logFile = new BufferedWriter(new FileWriter("./log_file.txt", false));
            } catch (IOException ex) {
                System.err.println(ex.getLocalizedMessage());
                logFile = new BufferedWriter(new OutputStreamWriter(System.out));
            }
        } else {
            logFile = new BufferedWriter(new OutputStreamWriter(System.out));
        }
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(timeWork);
                Car car = storeCar.getCar();
                try {
                    logFile.write(new Date().toString() + ": Dealer" + numberOfDealer
                            + ": Auto: " + car.getId() + " (Body: " + car.getBodyId() + ", Motor: "
                            + car.getMotorId() + ", Accessory: " + car.getAccessoryId() + ")\n");
                    logFile.flush();
                } catch (IOException ex) {
                    System.err.println(ex.getLocalizedMessage());
                }
            } catch (InterruptedException ex) {
                break;
            }
        }
    }
}
