package ru.nsu.ccfit.vershinin.factworkemul.threadpool;

import java.util.ArrayList;
import java.util.List;

public class ThreadPool {
    private final List<Thread> taskList = new ArrayList<>();

    public void addTask(Runnable t) {
        Thread th = new Thread(t);
        th.start();
        taskList.add(th);
    }

    public void closeThreads() {
        for (Thread th: taskList) {
            th.interrupt();
        }
    }
}
