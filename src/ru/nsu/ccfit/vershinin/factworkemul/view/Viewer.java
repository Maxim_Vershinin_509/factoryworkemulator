package ru.nsu.ccfit.vershinin.factworkemul.view;

import ru.nsu.ccfit.vershinin.factworkemul.controller.Controller;
import ru.nsu.ccfit.vershinin.factworkemul.factory.Factory;
import ru.nsu.ccfit.vershinin.factworkemul.observer.Observable;
import ru.nsu.ccfit.vershinin.factworkemul.observer.Observer;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.util.Map;
import java.util.TreeMap;

public class Viewer implements Observer, ChangeListener {
    private final Frame frame;
    private final Factory factory;
    private final Controller controller;
    private Map<Observable.StoreUpdater, Runnable> updaterRunnableMap;

    private void initUpdaterRunnableMap() {
        updaterRunnableMap = new TreeMap<>();
        updaterRunnableMap.put(Observable.StoreUpdater.COUNT_ACCESSORIES, () -> updateCountAccessories(factory.getCountAccessories()));
        updaterRunnableMap.put(Observable.StoreUpdater.COUNT_BODIES, () -> updateCountBodies(factory.getCountBodies()));
        updaterRunnableMap.put(Observable.StoreUpdater.COUNT_MOTORS, () -> updateCountMotors(factory.getCountMotors()));
        updaterRunnableMap.put(Observable.StoreUpdater.COUNT_CARS, () -> updateCountCars(factory.getCountCars()));
        updaterRunnableMap.put(Observable.StoreUpdater.STORE_ACCESSORY_COUNT, () -> updateStoreAccessoryCount(factory.getStoreAccessoryCount()));
        updaterRunnableMap.put(Observable.StoreUpdater.STORE_BODY_COUNT, () -> updateStoreBodyCount(factory.getStoreBodyCount()));
        updaterRunnableMap.put(Observable.StoreUpdater.STORE_MOTOR_COUNT, () -> updateStoreMotorCount(factory.getStoreMotorCount()));
        updaterRunnableMap.put(Observable.StoreUpdater.STORE_CAR_COUNT, () -> updateStoreCarCount(factory.getStoreCarCount()));
    }

    private void updateCountAccessories(int countAccessories) {
        frame.setCountAccessoriesLabel(countAccessories);
    }

    private void updateCountBodies(int countBodies) {
        frame.setCountBodiesLabel(countBodies);
    }

    private void updateCountMotors(int countMotors) {
        frame.setCountMotorsLabel(countMotors);
    }

    private void updateCountCars(int countCars) {
        frame.setCountCarsLabel(countCars);
    }

    private void updateStoreAccessoryCount(int storeCount) {
        frame.setStoreAccessoryCountLabel(storeCount);
    }

    private void updateStoreBodyCount(int storeCount) {
        frame.setStoreBodyCountLabel(storeCount);
    }

    private void updateStoreMotorCount(int storeCount) {
        frame.setStoreMotorCountLabel(storeCount);
    }

    private void updateStoreCarCount(int storeCount) {
        frame.setStoreCarCountLabel(storeCount);
    }

    public Viewer(Factory factory, Controller controller) {
        frame = new Frame(this);
        this.factory = factory;
        this.controller = controller;
        factory.registerObserver(this);
        frame.setVisible(true);
        initUpdaterRunnableMap();
    }

    void exit() {
        controller.exit();
    }

    @Override
    public void stateChanged(ChangeEvent changeEvent) {
        Object sourceSlider = changeEvent.getSource();
        if (sourceSlider instanceof JSlider) {
            JSlider slider = (JSlider) sourceSlider;
            String sliderName = slider.getName();
            if ("accessorySuppliersSlider".equals(sliderName)) {
                controller.setAccessorySuppliersTimeWork(slider.getValue());
            } else if ("bodySuppliersSlider".equals(sliderName)) {
                controller.setBodySuppliersTimeWork(slider.getValue());
            } else if ("motorSuppliersSlider".equals(sliderName)) {
                controller.setMotorSuppliersTimeWork(slider.getValue());
            } else if ("machineBuildersSlider".equals(sliderName)) {
                controller.setMachineBuildersTimeWork(slider.getValue());
            } else if ("dealersSlider".equals(sliderName)) {
                controller.setDealersTimeWork(slider.getValue());
            }
        }

    }

    @Override
    public void update(Observable.StoreUpdater storeUpdater) {
        updaterRunnableMap.get(storeUpdater).run();
        frame.repaint(storeUpdater);
    }


}

