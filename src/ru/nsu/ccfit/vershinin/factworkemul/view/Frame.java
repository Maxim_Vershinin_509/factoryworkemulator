package ru.nsu.ccfit.vershinin.factworkemul.view;

import ru.nsu.ccfit.vershinin.factworkemul.observer.Observable;

import javax.swing.*;
import java.awt.*;
import java.util.Dictionary;
import java.util.Hashtable;

class Frame extends JFrame {

    private JLabel countAccessoriesLabel= new JLabel("0");
    private JLabel countBodiesLabel = new JLabel("0");
    private JLabel countMotorsLabel = new JLabel("0");
    private JLabel countCarsLabel = new JLabel("0");

    private JLabel storeAccessoryCountLabel = new JLabel("0");
    private JLabel storeBodyCountLabel = new JLabel("0");
    private JLabel storeMotorCountLabel = new JLabel("0");
    private JLabel storeCarCountLabel = new JLabel("0");

    private Viewer viewer;


    private void initJSlider(JSlider jSlider, String name) {
        Dictionary<Integer, JLabel> labelDictionary = new Hashtable<>();
        labelDictionary.put(0, new JLabel("0"));
        labelDictionary.put(20000, new JLabel("20000"));
        jSlider.setMajorTickSpacing(1000);
        jSlider.setPreferredSize(new Dimension(400, 50));
        jSlider.setLabelTable(labelDictionary);
        jSlider.setPaintTicks(true);
        jSlider.setPaintLabels(true);
        jSlider.addChangeListener(viewer);
        jSlider.setName(name);
    }

    private void initFrame() {
        BoundedRangeModel boundedRangeModel = new DefaultBoundedRangeModel(1000, 0, 0, 20000);
        JSlider accessorySuppliersSlider = new JSlider(boundedRangeModel);
        boundedRangeModel = new DefaultBoundedRangeModel(1000, 0, 0, 20000);
        JSlider bodySuppliersSlider = new JSlider(boundedRangeModel);
        boundedRangeModel = new DefaultBoundedRangeModel(1000, 0, 0, 20000);
        JSlider motorSuppliersSlider = new JSlider(boundedRangeModel);
        boundedRangeModel = new DefaultBoundedRangeModel(1000, 0, 0, 20000);
        JSlider machineBuildersSlider = new JSlider(boundedRangeModel);
        boundedRangeModel = new DefaultBoundedRangeModel(1000, 0, 0, 20000);
        JSlider dealersSlider = new JSlider(boundedRangeModel);

        initJSlider(accessorySuppliersSlider, "accessorySuppliersSlider");
        initJSlider(bodySuppliersSlider, "bodySuppliersSlider");
        initJSlider(motorSuppliersSlider, "motorSuppliersSlider");
        initJSlider(machineBuildersSlider, "machineBuildersSlider");
        initJSlider(dealersSlider, "dealersSlider");

        GridBagConstraints c = new GridBagConstraints();
        c.gridheight = 1;
        c.gridwidth = 1;
        c.ipady = 10;

        JPanel panel = new JPanel();

        panel.setLayout(new GridBagLayout());
        panel.setPreferredSize(new Dimension(1200, 800));
        c.gridx = 0;
        c.gridy = 0;
        panel.add(new JLabel("Accessory Suppliers time work (milliseconds):"), c);
        c.gridx = 1;
        panel.add(accessorySuppliersSlider, c);
        c.gridx = 0;
        c.gridy = 1;
        panel.add(new JLabel("Body Suppliers time work (milliseconds):"), c);
        c.gridx = 1;
        panel.add(bodySuppliersSlider, c);
        c.gridx = 0;
        c.gridy = 2;
        panel.add(new JLabel("Motor Suppliers time work (milliseconds):"), c);
        c.gridx = 1;
        panel.add(motorSuppliersSlider, c);
        c.gridx = 0;
        c.gridy = 3;
        panel.add(new JLabel("Machine Builders time work (milliseconds):"), c);
        c.gridx = 1;
        panel.add(machineBuildersSlider, c);
        c.gridx = 0;
        c.gridy = 4;
        panel.add(new JLabel("Dealers time work (milliseconds):"), c);
        c.gridx = 1;
        panel.add(dealersSlider, c);

        c.gridy = 5;
        c.gridx = 0;
        panel.add(new JLabel("Count of accessories supplied:"), c);
        c.gridx = 1;
        panel.add(countAccessoriesLabel, c);
        c.gridy = 6;
        c.gridx = 0;
        panel.add(new JLabel("Count of bodies supplied:"), c);
        c.gridx = 1;
        panel.add(countBodiesLabel, c);
        c.gridy = 7;
        c.gridx = 0;
        panel.add(new JLabel("Count of motors supplied:"), c);
        c.gridx = 1;
        panel.add(countMotorsLabel, c);
        c.gridy = 8;
        c.gridx = 0;
        panel.add(new JLabel("Count of assembled cars:"), c);
        c.gridx = 1;
        panel.add(countCarsLabel, c);

        c.gridy = 9;
        c.gridx = 0;
        panel.add(new JLabel("Count of accessories on store"), c);
        c.gridx = 1;
        panel.add(storeAccessoryCountLabel, c);
        c.gridy = 10;
        c.gridx = 0;
        panel.add(new JLabel("Count of bodies on store"), c);
        c.gridx = 1;
        panel.add(storeBodyCountLabel, c);
        c.gridy = 11;
        c.gridx = 0;
        panel.add(new JLabel("Count of motors on store"), c);
        c.gridx = 1;
        panel.add(storeMotorCountLabel, c);
        c.gridy = 12;
        c.gridx = 0;
        panel.add(new JLabel("Count of cars on store"), c);
        c.gridx = 1;
        panel.add(storeCarCountLabel, c);

        add(panel);
        pack();
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setResizable(false);
        this.setLocationRelativeTo(null);

    }

    @Override
    public void dispose() {
        viewer.exit();
        super.dispose();
    }

    void repaint(Observable.StoreUpdater storeUpdater) {
        switch (storeUpdater) {
            case STORE_ACCESSORY_COUNT:
                storeAccessoryCountLabel.repaint();
                break;
            case STORE_BODY_COUNT:
                storeBodyCountLabel.repaint();
                break;
            case STORE_MOTOR_COUNT:
                storeMotorCountLabel.repaint();
                break;
            case STORE_CAR_COUNT:
                storeCarCountLabel.repaint();
                break;
            case COUNT_ACCESSORIES:
                countAccessoriesLabel.repaint();
                break;
            case COUNT_BODIES:
                countBodiesLabel.repaint();
                break;
            case COUNT_MOTORS:
                countMotorsLabel.repaint();
                break;
            case COUNT_CARS:
                countCarsLabel.repaint();
                break;
            default:
                break;
        }

    }

    void setCountAccessoriesLabel(int count) {
        this.countAccessoriesLabel.setText(Integer.toString(count));
    }

    void setCountBodiesLabel(int count) {
        this.countBodiesLabel.setText(Integer.toString(count));
    }

    void setCountMotorsLabel(int count) {
        this.countMotorsLabel.setText(Integer.toString(count));
    }

    void setCountCarsLabel(int count) {
        this.countCarsLabel.setText(Integer.toString(count));
    }

    void setStoreAccessoryCountLabel(int count) {
        this.storeAccessoryCountLabel.setText(Integer.toString(count));
    }

    void setStoreBodyCountLabel(int count) {
        this.storeBodyCountLabel.setText(Integer.toString(count));
    }

    void setStoreMotorCountLabel(int count) {
        this.storeMotorCountLabel.setText(Integer.toString(count));
    }

    void setStoreCarCountLabel(int count) {
        this.storeCarCountLabel.setText(Integer.toString(count));
    }

    Frame(Viewer viewer) throws HeadlessException {
        super("Factory");
        this.viewer = viewer;
        initFrame();
    }
}
